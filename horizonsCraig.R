require(aqp)
require(RODBC)
require(lattice)


#declare functions
mean.and.sd <- function(values){
  m <- mean(values, na.rm=TRUE)
  s <- sd(values, na.rm=TRUE)
  upper <- m + s
  lower <- m - s
  res <- c(lower=lower, mean=m, upper=upper, sd=s)
  return(res)
}

AP2Group<-function(index){
  return(range[index]>dataframe$MeanAnnualPrecipitation[i] & range[index+1]<=dataframe$MeanAnnualPrecipitation)
  
}

##create dataframe from horizons
dataframe = read.csv("Lab_data_for_Onn_3.csv",header = TRUE)

#select only record with caco3
dataframe = dataframe[complete.cases(dataframe['caco3']),]

#number of pedons for each ecosystem group
sumColums=table(dataframe$pedoniid,dataframe$Ecosystem)
sumColums =colSums(sumColums != 0)

group.list=lapply(X = unique(dataframe$Ecosystem),FUN = function(x) paste(strwrap(x,width=35),collapse = "\n"))

#promote to SoilProfileCollection
depths(dataframe)<- pedoniid ~ hzn_top.of.NCSS_Layer + hzn_bot.of.NCSS_Layer
site(dataframe) <- ~Ecosystem

b <- slab(dataframe, fm = Ecosystem ~ caco3, slab.structure = 1, strict = FALSE, slab.fun = mean.and.sd)


strip.text=mapply(FUN = function(x,y) paste0(x,'\n',y,' Sites'),group.list,as.character(sumColums))

#create plot
my.plot <- xyplot(top ~ mean | Ecosystem, data=b, main = "Craig DB \n Depth vs. mean CaCO3 \n grouped by annual precipitation",ylab = 'Depth [cm]', xlab = 'Mean CaCO3 concentration bounded by standard deviation[g/cm^2]',
                  lower=b$lower, upper=b$upper, sync.colors=TRUE, alpha=0.25,
                  cf=b$contributing_fraction, cf.col = 'black',
                  ylim=c(125,0), layout=c(3,1),
                  par.settings=list(layout.heights = list(strip=4),superpose.line=list(lwd=2, col=c('RoyalBlue', 'Orange2')),layout.widths = list(6)),
                  panel=panel.depth_function,
                  prepanel=prepanel.depth_function,
                  auto.key=list(columns=5, lines=TRUE, points=FALSE), par.strip.text = list(cex=0.7),
                  strip = strip.custom(factor.levels = paste(strip.text)))
my.plot
pdf(file = "soilPlotCraig.pdf")
print(my.plot)
dev.off()